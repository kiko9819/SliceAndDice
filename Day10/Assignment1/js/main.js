$(document).ready(function () {
    let $natureImageTop;
    let $natureImage = $(".nature-img");

    $.each($("button"), function (key, element) {
        $(this).on("click", function () {
            let btnNumber = $(this).attr("data-number");
            pickButton(parseInt(btnNumber,10));
        });
    });

    function pickButton(key) {
        if (key === 0) {
            $natureImage.hide();
        } else if (key === 1) {
            $natureImage.show();
        } else if (key === 2) {
            $natureImage.css({
                "border-radius": "2rem"
            });
        } else if (key === 3) {
            $natureImage.css({
                "border": "1rem solid red"
            });
        } else if (key === 4) {
            $natureImage.css({
                "border": "none"
            });
        } else {
            move(key);
        }
    }

    function move(key) {
        $natureImageTop = parseInt($natureImage.css("top"), 10);
        if ($natureImageTop === 0 && key === 5) {
            return;
        }
        if (key === 5) {
            $natureImageTop -= 10;
            $natureImage.css({
                "top": $natureImageTop + "px"
            });
        } else {
            $natureImageTop += 10;
            $natureImage.css({
                "top": $natureImageTop + "px"
            });
        }
    }
});