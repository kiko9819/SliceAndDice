$(document).ready(function () {
    $(".next").on("click", function () {
        let currentImg = $("img.active");
        let nextImg = currentImg.next();

        if (nextImg.length) {
            currentImg.removeClass("active").css("z-index", -10);
            nextImg.addClass("active").css("z-index", 10);
        } else {
            currentImg.removeClass("active").css("z-index", -10);
            $("img:first-of-type").addClass("active").css("z-index", 10);
        }
    });
    $(".prev").on("click", function () {
        let currentImg = $("img.active");
        let prevImg = currentImg.prev();

        if (prevImg.length) {
            currentImg.removeClass("active").css("z-index", -10);
            prevImg.addClass("active").css("z-index", 10);
        } else {
            currentImg.removeClass("active").css("z-index", -10);
            $("img:last-of-type").addClass("active").css("z-index", 10);
        }
    });
});