function menuToggler() {
    "use strict";
    let isClicked = false;
    let dropDownBtn = document.querySelector(".menu-drop-down-button");

    dropDownBtn.addEventListener("click", function () {
        let itemsList = document.querySelector(".unstyled-list");

        if (isClicked) {
            itemsList.style.display = "none";
            isClicked = !isClicked;
        } else {
            itemsList.style.display = "flex";
            isClicked = !isClicked;
        }
    });
}
menuToggler();