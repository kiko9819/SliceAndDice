let orderCount = 0;

function takeOrder(topping, crustType) {
    orderCount += 1;
    console.log("Order:" + crustType + " crust pizza topped with " + topping);
    console.log(getSubTotal(orderCount));
}

function getSubTotal(itemCount) {
    return itemCount * 7.5;
}

function getTax() {
    return getSubTotal(orderCount) * 0.06;
}

function getTotal() {
    return getSubTotal(orderCount) + getTax();
}
takeOrder("Bacon", "thin");
console.log(getTotal());