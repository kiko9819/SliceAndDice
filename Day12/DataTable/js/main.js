$(document).ready(function(){
    $("#users").DataTable({
        ajax: {
            url: "https://jsonplaceholder.typicode.com/users",
            dataSrc: ""
        },
        paging: false,
        searching: false,
        columns: [
            { data: "id" },
            { data: "name" },
            { data: "username" },
            { data: "email" },
            { data: "address.street" },
            { data: "phone" },
            { data: "website" },
            { data: "company.name" }
        ]
    });
});