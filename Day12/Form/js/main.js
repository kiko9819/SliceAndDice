$(document).ready(function () {
    $(".locations-menu").select2({
        placeholder: "Location"
    });
    $("#datetimepicker").datetimepicker({
        defaultDate: moment()
    });
});