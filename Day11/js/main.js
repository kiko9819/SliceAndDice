let infoLoaded = false;
$(".get-user-info").click(function () {
    if (!infoLoaded) {
        $.ajax({
            method: "GET",
            url: "https://jsonplaceholder.typicode.com/users",
            dataType: "json"
        }).done(function (data) {
            $.map(data, function (user, i) {
                $("#users tbody").append(
                    `
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.name}</td>
                        <td>${user.username}</td>
                        <td>${user.email}</td>
                        <td>st. ${user.address.street}, ${user.address.city}</td>
                        <td>${user.phone}</td>
                        <td>${user.website}</td>
                        <td>${user.company.name}</td>
                    </tr>
                `
                );
            });
        });
        infoLoaded = !infoLoaded;
    }
});