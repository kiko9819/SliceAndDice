google.charts.load('current', {
    'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawSeriesChart);

function drawSeriesChart() {
    let data = google.visualization.arrayToDataTable([
        ['ID', 'Students', 'Successful Students', 'Region', 'Schools'],
        ['CAN', 100, 78, 'North America', 100],
        ['DEU', 50, 43, 'Europe', 100],
        ['DNK', 70, 55, 'Europe', 100],
        ['EGY', 20, 19, 'Middle East', 100],
        ['GBR', 30, 30, 'Europe', 100],
        ['IRN', 35, 23, 'Middle East', 100],
        ['IRQ', 69, 40, 'Middle East', 100],
        ['ISR', 74, 70, 'Middle East', 100],
        ['RUS', 10, 9.54, 'Europe', 100],
        ['USA', 99, 88.05, 'North America', 100]
    ]);

    let options = {
        title: "Exam chart for top students of 2018 by country",
        hAxis: {
            title: "Exams"
        },
        vAxis: {
            title: "Successful Students"
        },
        bubble: {
            textStyle: {
                fontSize: 11
            }
        }
    };
    let chart = new google.visualization.BubbleChart(document.getElementById("ranking_char_div"));
    chart.draw(data, options);
}