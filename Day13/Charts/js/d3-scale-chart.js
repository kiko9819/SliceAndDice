let svgWidth = 600;
let svgHeight = 400;
let data = [20, 100, 50, 90, 900];

let svg = d3.select("svg.scale-chart")
    .attr("width", svgWidth)
    .attr("height", svgHeight);

let xScale = d3.scaleLinear()
    .domain([0, d3.max(data)])
    .range([0, svgWidth]);
let yScale = d3.scaleLinear()
    .domain([0, d3.max(data)])
    .range([svgHeight, 0]);

let x_axis = d3.axisBottom()
    .scale(xScale);
let y_axis = d3.axisLeft()
    .scale(yScale);

svg.append("g")
    .attr("transform", "translate(50,10)")
    .call(y_axis);

let xAxisTranslate = svgHeight - 20;

svg.append("g")
    .attr("transform", "translate(50," + xAxisTranslate + ")")
    .call(x_axis);

