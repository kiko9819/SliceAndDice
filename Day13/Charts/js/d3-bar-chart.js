let svgWidth = 500;
let svgHeight = 300;

let svg = d3.select("svg.bar-chart")
    .attr("width", svgWidth)
    .attr("height", svgHeight)
    .attr("class", "bar-chart");

let data = [45, 255, 89, 133];

let barMargin = 5;
let barWidth = svgWidth / data.length;

let barChart = svg.selectAll("rect")
    .data(data)
    .enter()
    .append("rect")
    .attr("y", function (dataPoint) {
        return svgHeight - dataPoint;
    })
    .attr("height", function (dataPoint) {
        return dataPoint;
    })
    .attr("width", barWidth - barMargin)
    .attr("transform", function (d, index) {
        let translate = [barWidth * index, 0];
        return "translate(" + translate + ")";
    })
    .attr("fill", "#14FF00");

let text = svg.selectAll("text")
    .data(data)
    .enter()
    .append("text")
    .text(function (dataPoint) {
        return dataPoint;
    })
    .attr("y", function (dataPoint, index) {
        return svgHeight - dataPoint - 2;
    })
    .attr("x", function (dataPoint, index) {
        return barWidth * index;
    })
    .attr("fill", "#fff");