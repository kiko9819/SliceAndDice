google.charts.load("current", {
    "packages": ["corechart"]
});
google.charts.setOnLoadCallback(drawDonut);

function drawDonut() {
    let data = google.visualization.arrayToDataTable([
        ["Task", "Hours per Day"],
        ["Eat", 5],
        ["Sleep", 10],
        ["Code", 80],
        ["Hangout with friends", 5]
    ]);

    let options = {
        title: "My Daily Activities",
        pieHole: 0.4
    };

    let chart = new google.visualization.PieChart(document.getElementById("donut_chart"));
    chart.draw(data, options);
}