$(document).ready(function () {
    $("#users").DataTable({
        ajax: {
            url: "https://jsonplaceholder.typicode.com/users",
            dataSrc: ""
        },
        paging: false,
        searching: false,
        columns: [
            { data: "id" },
            { data: "name" },
            { data: "username" },
            { data: "email" },
            { data: "address.street" },
            { data: "phone" },
            { data: "website" },
            { data: "company.name" }
        ]
    });
    $("#form").submit(function (event) {
        event.preventDefault();
        if (!$(this).valid()) {
            return;
        }
    });
    $("#form").validate({
        errorClass: "errors",
        rules: {
            name: {
                required: true,
                minlength: 4
            },
            username: {
                required: true,
                minlength: 4
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true,
                minlength: 6
            },
            phone: {
                required: true,
                minlength: 10
            },
            website: {
                required: false,
                minlength: 8
            },
            company: {
                required: false,
                minlength: 4
            }
        },
        messages: {
            name: {
                required: "Please enter your name"
            },
            username: {
                required: "Please enter your username"
            },
            email: {
                required: "Please enter your email"
            },
            address: {
                required: "Please enter your address"
            },
            phone: {
                required: "Please enter your phone number"
            }
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        submitHandler: function (form) {
            $.ajax({
                url: "https://jsonplaceholder.typicode.com/users",
                type: form.method,
                data: $(form).serialize(),
                success: function (response) {
                    let userData = JSON.parse(JSON.stringify($(form).serializeArray()));
                    let lastId = $("#users tr:last-of-type td:first-of-type").html();

                    $("#users").DataTable().row.add({
                        "id": Number(lastId) + 1,
                        "name": userData[0].value,
                        "username": userData[1].value,
                        "email": userData[2].value,
                        "address": {
                            "street": userData[3].value
                        },
                        "phone": userData[4].value,
                        "website": userData[5].value,
                        "company": {
                            "name": userData[6].value
                        }
                    }).draw();

                    PNotify.success({
                        text: "Successfully made a post request!!!"
                    });

                    $(form).find(".input-field").val("");
                },
                error: function () {
                    PNotify.error({
                        text: "Something went wrong"
                    });
                }
            });
        }
    });
});